# Grid Framework samples

This Unity3D package contains playable samples for [Grid Framework].


## Installation

This Git repository is a regular standalone Unity3D package. You can read more
about it in the [documentation].


## Usage

The package only contains samples. After having imported it open your Unity3D
package manager inside the editor, select this package, choose a sample and
then import it into your project. You can safely experiment with the and delete
it from your project, it will not affect the original sample from the package.


## License

Released under the MIT license. Please see the [LICENSE] file for details.


[Grid Framework]: http://hiphish.github.io/grid-framework/
[documentation]: Documentation~/com.hiphish.grid-framework.vectrosity.md
[LICENSE]: LICENSE.md
