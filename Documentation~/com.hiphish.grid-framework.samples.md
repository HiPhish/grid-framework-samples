# Grid Framework samples

## Package contents
The package contains only samples, no library code. Samples themselves have
their own assets: scripts, materials, and textures.


## Installation instructions
Install this package like any other Git dependency. Please refer to the Unity3D
manual for [more information](https://docs.unity3d.com/Manual/upm-git.html).


## Requirements
You need to have [Grid Framework] version 3 installed into your project.


## Samples
The following samples are included:

### Endless 2D grid
Create an illusion of an infinitely large grid by dynamically adjusting the
range of the grid as the camera moves.

### Endless 3D grid
Similar to the above, except in 3D.

### Level parsing
Construct the level of a puzzle game from data by dynamically placing blobs on
a grid.

### Lights out
A puzzle game in which toggling one light bulb toggles the adjacent light bulbs
as well.

### Movement
Move the hero from grid point to grid point, using data rather than collision
to detect impassable tiles.

### Rotary dial
A rotary phone dial that rotates up to the number the player has clicked.

### Sliding puzzle
A sliding puzzle using grid data rather than collision detection to limit the
movement of blocks.

### Snake
In this game of snake the segments follow one another over the grid.

### Snapping
Auto-snap blocks to the grid while dragging them like in a city builder game.

### Terrain mesh generation
Generate and manipulate a mesh whose vertices align with the vertices of the
grid.


[Grid Framework]: http://hiphish.github.io/grid-framework/
